{ pkgs ? import (fetchTarball {
    url = "https://github.com/NixOS/nixpkgs/archive/22.11.tar.gz";
    sha256 = "sha256:11w3wn2yjhaa5pv20gbfbirvjq6i3m7pqrq2msf0g7cv44vijwgw";
  }) {}
}:

let
  pyPkgs = pkgs.python3Packages;
in rec {
  ppredictor = pyPkgs.buildPythonPackage {
    pname = "ppredictor";
    version = "local";
    format = "pyproject";

    src = pkgs.lib.sourceByRegex ./. [
      "pyproject\.toml"
      "LICENSE"
      "ppredictor"
      "ppredictor/.*\.py"
      "ppredictor/cmd"
      "ppredictor/cmd/.*\.py"
    ];
    buildInputs = with pyPkgs; [
      flit
    ];
    propagatedBuildInputs = with pyPkgs; [
    ];
  };

  user-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
      ppredictor
    ];
  };

  dev-shell = pkgs.mkShell {
    buildInputs = with pyPkgs; [
      ipython
    ] ++ ppredictor.propagatedBuildInputs;
  };
}
