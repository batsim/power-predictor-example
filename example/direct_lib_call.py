#!/usr/bin/env python3
import ppredictor

def main():
    predictor = ppredictor.StupidPredictor()
    predictor.initialize({}, 0)
    predictor.on_job_submitted(0, 'w0!9', {
        "subtime":27,
        "walltime":100,
        "res":1,
        "user_id":"0",
    })
    predictor.on_job_completed(100, 'w0!9', {
        "job_state":"COMPLETED_SUCCESSFULLY",
        "return_code":0,
        "alloc":"1",
        "power_consumption_average": 1e9,
    })

    predictor.on_job_submitted(150, 'w0!10', {
        "subtime":150,
        "walltime":100,
        "res":1,
        "user_id":"0",
    })
    print(predictor.predict_submitted_job_features('w0!10'))

    print(predictor.predict_potential_job_features(150, {
        "subtime":150,
        "walltime":100,
        "res":1,
        "user_id":"0",
    }))

if __name__ == '__main__':
    main()
