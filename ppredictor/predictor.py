import abc

class Predictor(abc.ABC):
    @abc.abstractmethod
    def initialize(self, now: float, data: dict) -> None:
        pass

    @abc.abstractmethod
    def finalize(self, now: float) -> None:
        pass

    @abc.abstractmethod
    def on_job_submitted(self, now: float, job_id: str, job_submission_data: dict) -> None:
        pass

    @abc.abstractmethod
    def on_job_completed(self, now: float, job_id: str, job_execution_data: dict) -> None:
        pass

    @abc.abstractmethod
    def predict_submitted_job_features(self, now: float, job_id: str) -> dict:
        pass

    @abc.abstractmethod
    def predict_potential_job_features(self, now: float, potential_job_submission_data: dict) -> dict:
        pass
