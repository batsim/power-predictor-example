import statistics
from .predictor import Predictor

class StupidPredictor(Predictor):
    def initialize(self, now: float, data: dict) -> None:
        self.running_jobs = {}
        self.power_consumptions = []
        self.power_consumption_avg = float('NaN')
        self.power_consumption_sd = float('NaN')

    def finalize(self, now: float) -> None:
        pass

    def on_job_submitted(self, now: float, job_id: str, job_submission_data: dict) -> None:
        pass

    def on_job_completed(self, now: float, job_id: str, job_execution_data: dict) -> None:
        self.power_consumptions.append(job_execution_data['power_consumption_average'])
        self.power_consumption_avg = statistics.mean(self.power_consumptions)
        self.power_consumption_sd = statistics.pstdev(self.power_consumptions)

    def predict_submitted_job_features(self, job_id: str) -> dict:
        return {
            'power_consumption_average': self.power_consumption_avg,
            'power_consumption_sd': self.power_consumption_sd,
        }

    def predict_potential_job_features(self, now: float, potential_job_submission_data: dict) -> dict:
        return {
            'power_consumption_average': self.power_consumption_avg,
            'power_consumption_sd': self.power_consumption_sd,
        }
